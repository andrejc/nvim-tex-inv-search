# Script and NeoVim Plugin to do LaTeX Inverse Searches

A *LaTeX Inverse Search* is when you click (or control click) in your PDF
viewer, and it takes you to the corresponding location in the `tex` file.
This script/plugin provide the following functionality:

* Click / Control Click in your PDF viewer.
* If the source file is being edited in some [NeoVim] instance, then move to
  the corresponding location and briefly highlight the line.
* If the source file is not being edited by NeoVim, then launch it in some
  NeoVim frontend (`goneovim`, `nvim-qt`, `gnvim` in order of preference).

## Options

* `g:TexInvSearchHLTime`: Time (in seconds) to highlight the current line
  after an inverse search. If `0`, then highlight until the cursor is moved
  (default). If negative, then don't highlight. (Needs `'cursorline'` to be
  unset.)

## Installation

1. Clone the repository into `~/.config/nvim/pack/*/start/tex-inv-search`, or
   use `packer` / some other plugin manager.

2. Install [zsh](https://www.zsh.org/).

3. Create a symlink to `bin/nvim-inv-search` somewhere in your path:

        ln -srL .../tex-inv-search/bin/nvim-inv-search ~/bin/

4. Configure your PDF viewer (below)

### Configuring Zathura

Edit (or create) `~/.config/zathura/zathurarc` and add the option:

    set synctex-editor-command "nvim-inv-search %{line} '%{input}'"

### Configuring Okular

Open *Preferences -> Editor* and set `Command` to

    nvim-inv-search +%l %f

## Alternatives

There are a few other plugins that provide LaTeX inverse searches that may (or
may not) work better for you:

* [VimTex](https://github.com/lervag/vimtex) provides inverse searches and
  many other features. Unfortunately the inverse search was unreliable for me
  and sometimes required 10 clicks before responding. Also it doesn't work if
  you're currently editing a different file, and it doesn't open a new
  instance if you're not editing the file.

* [TexLabConfig](https://github.com/f3fora/nvim-texlabconfig)).
  I couldn't use this because it's hard to use only the inverse search from
  `TexLabConfig`, in addition to `VimTeX`/`coc.nvim` (which I need for other
  features).

## Issues

* If the running vim instance is in operator pending mode (e.g. after `f` or
  `d`, etc.), then it doesn't respond, and the source file may be incorrectly
  opened in a new window.

## LICENCE

Distributed under the MIT Licence.
Copyright (c) 2022 Gautam Iyer

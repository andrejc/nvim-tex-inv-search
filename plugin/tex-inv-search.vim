" NeoVim plugin for use with nvim-inv-search (Latex inverse search)
" Maintainer:	GI <gi1242+vim@nospam.com> (replace nospam with gmail)
" Created:	Wed 27 Jul 2022 08:23:27 PM EDT
" Last Changed:	Thu 28 Jul 2022 09:25:29 PM EDT
" Version:	0.1
"
" Description:
"   TODO: Write description here.

lua << EOF
_TexInvSearchCurTime = 0
function _TexInvSearch( line, buf )
  vim.schedule( function()
      vim.api.nvim_win_set_buf(0, buf )
      vim.api.nvim_win_set_cursor(0, {line, 0} )
      vim.api.nvim_feedkeys(
	vim.api.nvim_replace_termcodes( '<C-\\><C-N>zvzz', true, true, true ),
	'n', false )
      if vim.wo.cursorline == false or vim.b.TexInvSearchCanHL ~= nil then
	vim.cmd( 'doauto TexInvSearch CursorMoved' )
	vim.wo.cursorline = true
	vim.b.TexInvSearchCanHL = 1
	if vim.g.TexInvSearchHLTime > 0 then
	  _TexInvSearchCurTime = os.time()
	  vim.defer_fn( function()
	      if os.time() - _TexInvSearchCurTime >= vim.g.TexInvSearchHLTime
	      then
		vim.wo.cursorline = false
		vim.b.TexInvSearchCanHL = nil
	      end
	    end, vim.g.TexInvSearchHLTime*1000 )
	end
	vim.defer_fn( function()
	    vim.cmd( [[
	      augroup TexInvSearch
		autocmd!
		autocmd CursorMoved,ModeChanged * ++once setl nocursorline 
		  \ | unlet! b:TexInvSearchCanHL
	      augroup END
	    ]] )
	  end, 500 )
      end
  end)
end
EOF

function! TexInvSearch( line, file )
  let l:buf = getbufinfo(a:file)[0].bufnr
  call v:lua._TexInvSearch( a:line, l:buf )
endfunction

if !exists('g:TexInvSearchHLTime')
  let g:TexInvSearchHLTime = 0
endif

augroup TexInvSearch
  autocmd!
augroup END

" vim: set sw=2 :

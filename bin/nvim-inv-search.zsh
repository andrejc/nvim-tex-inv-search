#! /bin/zsh
# Created	: Wed 08 Feb 2006 10:40:21 AM CST
# Modified	: Thu 28 Jul 2022 02:53:19 PM EDT
# Author	: Gautam Iyer <a@b.c> where a=gi1242, b=gmail, c=com
# Description	: 
# Usage		: nvim-inv-search <line> <file>

# 2016-01-17: Sometime Vim leaks memeory and gobbles everything. Limit memory
# usage of all subprocesses to 2Gb
#set -o xtrace
set -o extendedglob
ulimit -Sv 4000000

function debug() {
    [[ -n $DEBUG ]] && echo "$@" >> /dev/stderr
}

function tryServer() {
    # Check if file is being edited in an already running vim server
    # If yes, open it, and open current fold
    s=$1
    chkLoaded=(timeout 2
	nvim --server $s --remote-expr "bufloaded('$fileName')")
    if [[ $($chkLoaded 2>&1 ) == 1 ]]; then
	debug "Found $fileName in server $s"
	rm -f $tmpfile
	# Console vim sessions need to be manually raised.
	case $XDG_CURRENT_DESKTOP in
	    (fvwm2)
		# This needs the servername to be the last word (space
		# preceded) in the title (default on vim).
		FvwmCommand "Next (konsole, '* - $s — Konsole') WindowListFunc"
		;;
	esac
	#exec nvim --server $s --remote "$@" --remote-send '<esc>zv'
	exec nvim --server $s --remote-expr \
	    "TexInvSearch($line, '$fileName')"
	debug "Error running nvim server $s"
    fi
}

line=$1
fileName=$2

typeset -aU vimServers=(/tmp/nvim*/0(N))

tmpfile=$(mktemp /tmp/inv-search-${fileName:t}-XXXXXXXX)
for server in $vimServers; do
    debug "Trying server $server"
    tryServer $server &
done
wait

if [[ -e  $tmpfile ]]; then
    ## If we got here, then we couldn't find a matching server. Start a new one.
    rm -f $tmpfile
    for c in goneovim nvim-qt gnvim; do
	if [[ -n $commands[$c] ]]; then
	    exec $c -- $fileName -c "call TexInvSearch($line, '$fileName')"
	    debug "Error executing $c"
	    exit 1
	fi
    done
    debug "No GUI frontends for NeoVim found"
    exit 1
fi
